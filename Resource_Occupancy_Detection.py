import psutil
import matplotlib.pyplot as plt
import time,sys
if len(sys.argv) > 1:
    DELAY=sys.argv[1]
else:
    DELAY=5
cpu,memory=[],[]
t1=time.time()
try:
    while True:
        print("Sampling...")
        cpu.append(psutil.cpu_percent())
        memory.append(psutil.virtual_memory().percent)
        time.sleep(DELAY)
except:
    t2=time.time()
    y=[]
    print("In drawing....")
    plt.grid(True)
    for i in range(0,int(t2)-int(t1),5):
        y.append(int(i))
    plt.plot(y,cpu,label='CPU')
    plt.xlabel('time / sec')
    plt.ylabel('Percent / %')
    plt.plot(y,memory,label='Memory')
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=0,ncol=3, mode="expand", borderaxespad=0.)
    print('saving.....')
    plt.savefig(time.strftime("%Y-%m-%d_%H-%M-%S", time.localtime())+'.png')
    plt.show()
