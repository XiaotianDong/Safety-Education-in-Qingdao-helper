import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox as mbx
import tkinter.filedialog 
import selenium.webdriver
from selenium.webdriver.common.by import By
import time
import xlrd
import threading
#初始化变量
value=[]
filepath=""
logfile=open(time.strftime("%Y-%m-%d_%H-%M-%S", time.localtime())+'.log','w')
#获取浏览器对象
try:
    driver=selenium.webdriver.Chrome()
    logfile.write("Using Browser: Chrome\n")
except:
    try:
        driver=selenium.webdriver.Firefox()
        logfile.write("Using Browser: Firefox\n")
    except:
        mbx.showerror(title="教育平台助手",message="初始化失败，5秒后退出")
        logfile.write("deadly:Browser initialization failed")
        time.sleep(5)
        exit()
#初始化函数
def openfile(file_path):
    logfile.write("Opening File:"+file_path+"\n")
    value.clear()
    data  = xlrd.open_workbook(file_path) #打开excel
    table = data.sheet_by_name("Sheet1") #读sheet
    nrows = table.nrows
    cols = table.ncols
    for i in range(0,nrows): 
        for j in range(cols):
            ctype = table.cell(i , j).ctype
            no = table.cell(i, j).value
            if ctype == 2 :
                no=str(int(no))
                value.append(no)
            if ctype == 1:
                value.append(no)
def bind_chosefile_button():
    try:    
        file_path=tk.filedialog.askopenfilename()
        label_filepath.config(text=file_path)
        openfile(file_path)
    except:
        tk.messagebox.showerror(title="教育平台",message="打开失败")
        logfile.write("deadly:Open file ERROR")
def main(user,password):
    wait_time=0
    homework_list=[]
    homework_title_list=[]
    temp=[]
    waittimeouterror=False
    driver.get(r"https://qingdao.xueanquan.com/Login.html")
    driver.implicitly_wait(30)
    driver.find_element_by_id('UName').clear()
    driver.find_element_by_id('UName').send_keys(user)
    driver.find_element_by_id('PassWord').clear()
    driver.find_element_by_id('PassWord').send_keys(password)
    driver.find_element_by_id('LoginButton').click()
    while driver.current_url!=r"https://qingdao.xueanquan.com/MainPage.html":
        wait_time+=1
        time.sleep(1)
        if wait_time >=30:break
    driver.get("https://qingdao.xueanquan.com/JiaTing/JtMyHomeWork.html")
    driver.implicitly_wait(30)
    driver.execute_script(r"getMun(9,$(this))")
    time.sleep(2)
    temp1=driver.find_elements_by_css_selector(r"tr.tr_wwc td > div.wca > a")
    time.sleep(2)
    driver.execute_script(r"goPage('mun_-1',2,2)")
    temp2=driver.find_elements_by_css_selector(r"tr.tr_wwc td > div.wca > a")
    for i in temp1:
        temp.append(i)
    for i in temp2:
        temp.append(i)
    if not temp:
        status_bar.config(text="EMPTY USER:"+user)
        logfile.write("EMPTY USER:"+user)
        logfile.write('\n')
    for i in temp:
        homework_list.append(i.get_attribute("onclick"))
        homework_title_list.append(i.get_attribute("title"))
    for i in range(len(homework_list)-1):
        try:
            driver.execute_script(homework_list[i])
            while wait_time <=10:
                wait_time+=1
                if len(driver.window_handles) ==1:
                    waittimeouterror=True
                    break
            if waittimeouterror:
                status_bar.config(text="END HOMEWORK: "+homework_title_list[i])
                logfile.write("END HOMEWORK: "+homework_title_list[i])
                logfile.write('\n')
                waittimeouterror=False
                continue
            if not waittimeouterror:
                run(driver,homework_title_list[i])
            waittimeouterror=False
        except:
            status_bar.config(text="ERROR HOMEWORK: "+homework_title_list[i])
            logfile.write("ERROR HOMEWORK: "+homework_title_list[i])
            logfile.write('\n')
def bind_start_button(srarus_bar):
    for i in range(0,len(value),2):
        try:
            logfile.write('Doing User:'+value[i])
            main(value[i],value[i+1])
        except:
            status_bar.config(text="ERROR UESR:"+value[i])
            logfile.write("ERROR UESR:"+value[i])
            logfile.write('\n')
        else:
            status_bar.config(text="NORMAL UESR:"+value[i])
            logfile.write("NORMAL UESR:"+value[i])
            logfile.write('\n')
    mbx.showinfo(title="教育平台助手",message="完成")
def run(driver,homework_name):
    if homework_name =="教育孩子感恩大自然、爱护大自然":
        driver.switch_to_window(driver.window_handles[1])
        time.sleep(2)
        driver.find_element_by_css_selector(r"#buzhou2ss").click()
        driver.find_element_by_css_selector("#test8586 > dd:nth-child(2) > input").click()
        driver.find_element_by_css_selector("#test8587 > dd:nth-child(2) > input").click()
        driver.find_element_by_css_selector("#test8588 > dd:nth-child(2) > input").click()
        driver.find_element_by_css_selector("#test8588 > dd:nth-child(2) > input").click()
        driver.find_element_by_css_selector("#test8589 > dd:nth-child(2) > input").click()
        driver.find_element_by_css_selector("#test8590 > dd:nth-child(2) > input").click()
        driver.find_element_by_id("input_button").click()
        time.sleep(3)
        driver.find_element_by_css_selector(".icon img").click()
        driver.close()
        driver.switch_to_window(driver.window_handles[0])
        status_bar.config(text=homework_name+" is finish")
        logfile.write(homework_name+" is finish")
        logfile.write('\n')
    elif homework_name == "帮助孩子远离网络陷阱":
        driver.switch_to_window(driver.window_handles[1])
        time.sleep(2)
        driver.find_element_by_css_selector(r"#buzhou2ss").click()
        driver.find_element(By.CSS_SELECTOR, "#test9177 > dd:nth-child(2) > input").click()
        driver.find_element(By.NAME, "radio1_1").click()
        driver.find_element(By.NAME, "radio2_2").click()
        driver.find_element(By.NAME, "radio3_3").click()
        driver.find_element(By.NAME, "radio4_4").click()
        driver.find_element(By.ID, "input_button").click()
        driver.close()
        driver.switch_to_window(driver.window_handles[0])
        status_bar.config(text=homework_name+" is finish")
        logfile.write(homework_name+" is finish")
        logfile.write('\n')
    elif homework_name == "教导孩子拒绝吸烟酗酒":
        driver.switch_to_window(driver.window_handles[1])
        time.sleep(2)
        driver.find_element_by_css_selector(r"#buzhou2ss").click()
        driver.find_element(By.CSS_SELECTOR, "#test9184 > dd:nth-child(3) > input").click()
        driver.find_element(By.CSS_SELECTOR, "#test9185 > dd:nth-child(3) > input").click()
        driver.find_element(By.NAME, "radio2_2").click()
        driver.find_element(By.NAME, "radio3_3").click()
        driver.find_element(By.CSS_SELECTOR, "#test9188 > dd:nth-child(2) > input").click()
        driver.find_element(By.ID, "input_button").click()
        driver.close()
        driver.switch_to_window(driver.window_handles[0])
        status_bar.config(text=homework_name+" is finish")
        logfile.write(homework_name+" is finish")
    elif homework_name == "远离野外活动中的意外伤害":
        driver.switch_to_window(driver.window_handles[1])
        time.sleep(2)
        driver.find_element_by_css_selector(r"#buzhou2ss").click()
        driver.find_element(By.CSS_SELECTOR, "#test9192 > dd:nth-child(2) > input").click()
        driver.find_element(By.CSS_SELECTOR, "#test9193 > dd:nth-child(2) > input").click()
        driver.find_element(By.CSS_SELECTOR, "#test9194 > dd:nth-child(2) > input").click()
        driver.find_element(By.NAME, "radio3_3").click()
        driver.find_element(By.NAME, "radio4_4").click()
        driver.find_element(By.ID, "input_button").click()
        driver.close()
        driver.switch_to_window(driver.window_handles[0])
        status_bar.config(text=homework_name+" is finish")
        logfile.write(homework_name+" is finish")
        logfile.write('\n')
    elif homework_name == "家长帮助孩子远离校园中的意外伤害":
        driver.switch_to_window(driver.window_handles[1])
        time.sleep(2)
        driver.find_element_by_css_selector(r"#buzhou2ss").click()
        driver.find_element(By.CSS_SELECTOR, "#test9199 > dd:nth-child(3) > input").click()
        driver.find_element(By.NAME, "radio1_1").click()
        driver.find_element(By.CSS_SELECTOR, "#test9201 > dd:nth-child(2) > input").click()
        driver.find_element(By.NAME, "radio3_3").click()
        driver.find_element(By.CSS_SELECTOR, "#test9203 > dd:nth-child(3) > input").click()
        driver.find_element(By.ID, "input_button").click()
        driver.close()
        driver.switch_to_window(driver.window_handles[0])
        status_bar.config(text=homework_name+" is finish")
        logfile.write(homework_name+" is finish")
    elif homework_name == "学会安全用药":
        driver.switch_to_window(driver.window_handles[1])
        time.sleep(2)
        driver.find_element_by_css_selector(r"#buzhou2ss").click()
        driver.find_element(By.NAME, "radio0_0").click()
        driver.find_element(By.CSS_SELECTOR, "#test9208 > dd:nth-child(3) > input").click()
        driver.find_element(By.CSS_SELECTOR, "#test9209 > dd:nth-child(2) > input").click()
        driver.find_element(By.CSS_SELECTOR, "#test9210 > dd:nth-child(3) > input").click()
        driver.find_element(By.CSS_SELECTOR, "#test9211 > dd:nth-child(3) > input").click()
        driver.find_element(By.ID, "input_button").click()
        driver.close()
        driver.switch_to_window(driver.window_handles[0])
        status_bar.config(text=homework_name+" is finish")
        logfile.write(homework_name+" is finish")
        logfile.write('\n')
    elif homework_name == "排除家庭安全隐患":
        driver.switch_to_window(driver.window_handles[1])
        time.sleep(2)
        driver.find_element_by_css_selector(r"#buzhou2ss").click()
        driver.find_element(By.CSS_SELECTOR, "#test9214 > dd:nth-child(2) > input").click()
        driver.find_element(By.CSS_SELECTOR, "#test9215 > dd:nth-child(2) > input").click()
        driver.find_element(By.CSS_SELECTOR, "#test9216 > dd:nth-child(2) > input").click()
        driver.find_element(By.NAME, "radio3_3").click()
        driver.find_element(By.CSS_SELECTOR, "#test9218 > dd:nth-child(2) > input").click()
        driver.find_element(By.ID, "input_button").click()
        driver.close()
        driver.switch_to_window(driver.window_handles[0])
        status_bar.config(text=homework_name+" is finish")
        logfile.write(homework_name+" is finish")
    elif homework_name == "学会应对敲诈、抢劫、绑架、恐吓":
        driver.switch_to_window(driver.window_handles[1])
        time.sleep(2)
        driver.find_element_by_css_selector(r"#buzhou2ss").click()
        driver.find_element(By.NAME, "radio0_0").click()
        driver.find_element(By.NAME, "radio1_1").click()
        driver.find_element(By.NAME, "radio2_2").click()
        driver.find_element(By.CSS_SELECTOR, "#test9224 > dd:nth-child(2) > input").click()
        driver.find_element(By.CSS_SELECTOR, "#test9225 > dd:nth-child(2) > input").click()
        driver.find_element(By.ID, "input_button").click()
        driver.close()
        driver.switch_to_window(driver.window_handles[0])
        status_bar.config(text=homework_name+" is finish")
        logfile.write(homework_name+" is finish")
        logfile.write('\n')
    elif homework_name == "提高警惕防被骗":
        driver.switch_to_window(driver.window_handles[1])
        time.sleep(2)
        driver.find_element_by_css_selector(r"#buzhou2ss").click()
        driver.find_element(By.CSS_SELECTOR, "#test9230 > dd:nth-child(2) > input").click()
        driver.find_element(By.CSS_SELECTOR, "#test9231 > dd:nth-child(3) > input").click()
        driver.find_element(By.CSS_SELECTOR, "#test9232 > dd:nth-child(2) > input").click()
        driver.find_element(By.NAME, "radio3_3").click()
        driver.find_element(By.CSS_SELECTOR, "#test9234 > dd:nth-child(3) > input").click()
        driver.find_element(By.ID, "input_button").click()
        driver.close()
        driver.switch_to_window(driver.window_handles[0])
        status_bar.config(text=homework_name+" is finish")
        logfile.write(homework_name+" is finish")
        logfile.write('\n')
    elif homework_name == "预防与应对校园暴力":
        driver.switch_to_window(driver.window_handles[1])
        time.sleep(2)
        driver.find_element_by_css_selector(r"#buzhou2ss").click()
        driver.find_element(By.CSS_SELECTOR, "#test9237 > dd:nth-child(3) > input").click()
        driver.find_element(By.CSS_SELECTOR, "#test9239 > dd:nth-child(2) > input").click()
        driver.find_element(By.CSS_SELECTOR, "#test9240 > dd:nth-child(3) > input").click()
        driver.find_element(By.CSS_SELECTOR, "#test9241 > dd:nth-child(2) > input").click()
        driver.find_element(By.CSS_SELECTOR, "#test9242 > dd:nth-child(3) > input").click()
        driver.find_element(By.ID, "input_button").click()
        driver.close()
        driver.switch_to_window(driver.window_handles[0])
        status_bar.config(text=homework_name+" is finish")
        logfile.write(homework_name+" is finish")
        logfile.write('\n')
    else:
        driver.switch_to_window(driver.window_handles[1])
        driver.close()
        driver.switch_to_window(driver.window_handles[0])
        status_bar.config(text=homework_name+" is not suopport")
        logfile.write(homework_name+" is not suopport")
        logfile.write('\n')
def threads(func,*args):
    t = threading.Thread(target=func,args=args)
    t.setDaemon(True) 
    t.start()
#初始化主窗口
root=tk.Tk();root.title("教育平台助手")
#初始化组件
#状态栏显示
status_bar=ttk.Label(root,text='Ready.')
logfile.write('ready.\n')
status_bar.grid(row=3)
#文件提示标签
ttk.Label(root,text="文件路径").grid(row=1,column=0)
#文件路径显示
label_filepath=ttk.Label(root)
label_filepath.grid(row=1,column=1)
#选择按钮
ttk.Button(root,text="选择文件",command=lambda:bind_chosefile_button()).grid(row=1,column=3)
#开始按钮
ttk.Button(root,text="开始",command=lambda:threads(bind_start_button,status_bar)).grid(row=2,column=1)
#退出按钮
ttk.Button(root,text='退出',command=root.destroy).grid(row=2,column=2)
#start loop
root.mainloop()